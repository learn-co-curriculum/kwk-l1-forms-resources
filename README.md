##Forms resources

* [HTML Forms](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Forms)
* [GET and POST requests](http://www.w3schools.com/tags/ref_httpmethods.asp)

<p data-visibility='hidden'>KWK-L1 Forms resources</p>